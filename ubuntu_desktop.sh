
#mirror server change
sudo perl -pi -e 's/kr.archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list
sudo apt-get update

#install vim
sudo apt-get install vim
sudo apt-get remove vim-tiny

#ssh
perl -pi -e 's/^(\s*GSSAPI)/#$1/g' /etc/ssh/ssh_config

#oracle java install
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java7-installer

#
./install.sh
